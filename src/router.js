import { createRouter, createWebHistory } from "vue-router";
import App from "./App.vue";
import StudentData from "./components/StudentData.vue";
import Login from "./components/Login.vue";
import StudentSignUp from "./components/StudentSignUp.vue";
import TutorSignUp from "./components/TutorSignUp";
import HomeStudent from "./components/HomeStudent.vue";
import HomeTutor from "./components/HomeTutor.vue";
import TutorData from "./components/TutorData.vue";
import FAQ from "./components/FAQ.vue";  

const routes = [
  {
    path: "/",
    name: "root",
    component: App,
  },
  {
    path: "/user/login",
    name: "logIn",
    component: Login,
  },
  {
    path: "/user/signup/student",
    name: "studentSignUp",
    component: StudentSignUp,
  },
  {
    path: "/user/signup/tutor",
    name: "tutorSignUp",
    component: TutorSignUp,
  },
  {
    path: "/user/homeStudent",
    name: "homeStudent",
    component: HomeStudent,
  },
  ,
  {
    path: "/user/studentData",
    name: "studentData",
    component: StudentData,
  },
  {
    path: "/user/tutorData",
    name: "tutorData",
    component: TutorData,
  },
  {
    path: "/user/homeTutor",
    name: "homeTutor",
    component: HomeTutor,
  },
  {
    path: "/faq",
    name: "faq",
    component: FAQ,
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});
export default router;
